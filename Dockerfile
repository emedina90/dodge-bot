FROM php:8.1-cli

# Install dependencies
RUN apt-get update && apt-get install -y \
    unzip \
    libzip-dev \
    && docker-php-ext-install zip

# Install Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Set working directory
WORKDIR /app

# Copy composer.json and install dependencies
COPY composer.json ./
RUN composer install

# Copy the rest of the code
COPY . .

# Command to run on container start
CMD [ "php", "src/main.php" ]