<?php

namespace Ericmedina\DodgeBot\listeners;

use Discord\Discord;
use Discord\Parts\WebSockets\PresenceUpdate;

interface ExecutesPresenceListeners
{
    public function executeListeners(PresenceUpdate $presenceUpdate, Discord $discord, ?PresenceUpdate $oldPresence);
}