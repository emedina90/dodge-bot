<?php

namespace Ericmedina\DodgeBot\listeners;

use Discord\Discord;
use Discord\Parts\WebSockets\PresenceUpdate;
use Ericmedina\DodgeBot\listeners\presences\PresenceListener;

class DiscordExecutesPresenceListeners implements ExecutesPresenceListeners
{
    const LISTENERS_DIR       = __DIR__ . '/presences';
    const LISTENERS_NAMESPACE = '\\Ericmedina\\DodgeBot\\listeners\\presences\\';

    public function executeListeners(PresenceUpdate $presenceUpdate, Discord $discord, ?PresenceUpdate $oldPresence)
    {
        if (!is_dir(self::LISTENERS_DIR)) {
            throw new ListenersDirectoryMissingException();
        }

        $files   = scandir(self::LISTENERS_DIR);

        foreach ($files as $file) {
            $listener = $this->getListenerClassFromFile($file);

            if (empty($listener)) {
                continue;
            }

            $listener->execute($presenceUpdate, $discord, $oldPresence);
        }
    }

    private function getListenerClassFromFile($file): ?PresenceListener
    {
        $className          = explode('.php', $file)[0] ?? null;
        $fullyQualifiedName = self::LISTENERS_NAMESPACE . $className;

        if (!class_exists($fullyQualifiedName)) {
            return null;
        }

        return new $fullyQualifiedName();
    }
}