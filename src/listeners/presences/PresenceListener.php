<?php

namespace Ericmedina\DodgeBot\listeners\presences;

use Discord\Discord;
use Discord\Parts\WebSockets\PresenceUpdate;
use React\Promise\PromiseInterface;

interface PresenceListener
{
    public function execute(PresenceUpdate $presenceUpdate, Discord $discord, ?PresenceUpdate $oldPresence): ?PromiseInterface;
}