<?php

namespace Ericmedina\DodgeBot\listeners\presences;

use Carbon\Carbon;
use Discord\Discord;
use Discord\Parts\Channel\Channel;
use Discord\Parts\User\Member;
use Discord\Parts\WebSockets\PresenceUpdate;
use React\Promise\PromiseInterface;

class GamePresenceUpdateNotifier implements PresenceListener
{
    private const NOTIFICATIONS = [
        'no gays' => [
            'general' => [
                'apex'          => [ 'yeyo', 'NMBRS', 'flaterkk' ],
                'custom status' => [ 'yeyo', 'NMBRS', 'flaterkk' ],
            ]
        ],
        'greater nation mode' => [
            'greater-nation' => [
                'apex'          => [ 'yeyo', 'NMBRS', 'flaterkk' ],
            ]
        ]
    ];

    public function execute(PresenceUpdate $presenceUpdate, Discord $discord, ?PresenceUpdate $oldPresence): ?PromiseInterface
    {
        if (!$this->shouldListenerRun($presenceUpdate, $oldPresence, $discord)) {
            return null;
        }

        $guildName     = strtolower($presenceUpdate->guild->name);
        $notifications = self::NOTIFICATIONS[$guildName] ?? [];

        foreach ($notifications as $channelName => $games) {
            $discordChannel = $presenceUpdate->guild->channels->find(function(Channel $channel) use ($channelName) {
                return $channel->type === 0 && strtolower($channel->name) == $channelName;
            });

            if (empty($discordChannel)) continue;

            $gameName          = strtolower($presenceUpdate->game->name ?? '');
            $resolvedGameName  = $this->resolveGameName($gameName);
            $userNamesToNotify = $games[$resolvedGameName] ?? [];

            $membersToNotify   = array_filter(array_map(function($userName) use ($presenceUpdate, $gameName) {
                if (
                    $gameName !== 'custom status' && // For debugging
                    $presenceUpdate->user->username === $userName
                ) {
                    return null;
                }

                return $presenceUpdate->guild->members->get('username', $userName);
            }, $userNamesToNotify));

            if (empty($membersToNotify)) return null;

            $message = "{$presenceUpdate->user} is playing {$presenceUpdate->game->name}! Who's in!?";
            /** @var Member $member */
            foreach ($membersToNotify as $member) {
                $message .= " $member->user";
            }

            echo "$message\n";

//            $discordChannel->sendMessage($message);
        }

        return null;
    }

    private function resolveGameName(string $name): ?string
    {
        if (str_contains($name, 'apex')) {
            return 'apex';
        } else if (str_contains($name, 'custom status')) {
            return 'custom status';
        }

        return null;
    }

    private function shouldListenerRun(PresenceUpdate $presenceUpdate, ?PresenceUpdate $oldPresence, Discord $discord): bool
    {
        echo "New Update: \n";
        echo json_encode($discord->user->jsonSerialize());


        if (is_null($oldPresence)) {
            return true;
        }

        return $presenceUpdate->game?->name !== $oldPresence->game?->name;
    }
}