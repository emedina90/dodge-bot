<?php namespace Ericmedina\DodgeBot\listeners;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Ericmedina\DodgeBot\listeners\messages\MessageListener;

class DiscordExecutesMessageListeners implements ExecutesMessageListeners
{
    const LISTENERS_DIR       = __DIR__ . '/messages';
    const LISTENERS_NAMESPACE = '\\Ericmedina\\DodgeBot\\listeners\\messages\\';

    /**
     * Routes all incoming messages to each strategy in the /strategies folder
     * Where an Action will be executed
     */
    public function executeListeners(Message $message, Discord $discord)
    {
        if ($message->user_id === $_ENV['DISCORD_BOT_ID']) {
            return;
        }

        if (!is_dir(self::LISTENERS_DIR)) {
            throw new ListenersDirectoryMissingException();
        }

        $files   = scandir(self::LISTENERS_DIR);

        foreach ($files as $file) {
            $listener = $this->getListenerClassFromFile($file);

            if (empty($listener)) {
                continue;
            }

            $listener->execute($message, $discord);
        }
    }

    private function getListenerClassFromFile($file): ?MessageListener
    {
        $className          = explode('.php', $file)[0] ?? null;
        $fullyQualifiedName = self::LISTENERS_NAMESPACE . $className;

        if (!class_exists($fullyQualifiedName)) {
            return null;
        }

        return new $fullyQualifiedName();
    }
}