<?php namespace Ericmedina\DodgeBot\listeners;

use Discord\Discord;
use Discord\Parts\Channel\Message;

interface ExecutesMessageListeners {
    public function executeListeners(Message $message, Discord $discord);
}