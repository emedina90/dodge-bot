<?php

namespace Ericmedina\DodgeBot\listeners\messages;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\services\TarsServiceContract;
use React\Promise\PromiseInterface;

class TarsListener implements MessageListener
{
    public function execute(Message $message, Discord $discord): ?PromiseInterface
    {
        if (!$this->isAllowed($message)) {
            Logger::log("Not allowed to use TARS.");
            return null;
        }

        $hasTarsInString = $this->parseTarsString($message->content);

        if (!$hasTarsInString) {
            return null;
        }

        Logger::log("TARS detected. Dispatching.");

        /** @var TarsServiceContract $tarsService */
        $tarsService = Container::get(TarsServiceContract::class);
        $tarsService->dispatch($message);

        $message->react('⌛');

        return null;
    }

    private function parseTarsString(string $tarsString): bool
    {
        $regex = '/\b(tars)\b/i';
        $matches = [];
        preg_match($regex, $tarsString, $matches);

        return !empty($matches);
    }

    private function isAllowed(Message $message): bool
    {
        $featureFlags = include __DIR__ . '/../../config/feature_flags.php';

        return $featureFlags['tars']['allowed_guilds'][$message->guild_id] ?? false;
    }
}