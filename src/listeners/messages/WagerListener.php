<?php

namespace Ericmedina\DodgeBot\listeners\messages;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\services\BetService;
use React\Promise\PromiseInterface;
use function React\Async\async;

class WagerListener implements MessageListener
{
    public function execute(
        Message $message,
        Discord $discord
    ): ?PromiseInterface {
        return async(function () use ($message, $discord) {
            if (!str_starts_with($message->content, "!wager")) {
                return null;
            }

            $wager = $this->parseWagerString($message->content);

            if (empty($wager)) {
                return $message->reply(
                    "I was unable to recognize your wager. Try something like `!wager 5 on the Lakers`."
                );
            }

            /** @var BetService $betService */
            $betService = Container::get(BetService::class);
            $userName = $message->author->username;
            $guildId = $message->guild_id;

            [$wager, $team] = $wager;

            $betService->processWager($guildId, $userName, $wager, $team)->then(
                function ($result) use ($message) {
                    $message->reply($result);
                },
                function (\Exception $e) {
                    echo "Error: {$e->getMessage()}\n";
                }
            );

            return null;
        })();
    }

    private function parseWagerString(string $wagerString): array
    {
        // Parse strings that look like this: !wager 5 on the Lakers or !wager 5 on Lakers or !wager 5 Lakers
        // and return an array of the 5 and "Lakers"
        $matches = [];
        preg_match("/!wager (\d+)(?: on)? (.+)/", $wagerString, $matches);

        if (count($matches) === 0) {
            return [];
        }

        return [$matches[1], $matches[2]];
    }
}
