<?php

namespace Ericmedina\DodgeBot\listeners\messages;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\services\BetService;
use React\Promise\PromiseInterface;

class PointsListener implements MessageListener
{

    public function execute(Message $message, Discord $discord): ?PromiseInterface
    {
        /** @var BetService $betService */
        $betService = Container::get(BetService::class);

        if ($message->content === "!points") {
            $betService->getPlayerPoints($message->guild_id, $message->author->username)->then(function ($result) use ($message) {
                $reply = "You have $result points so far!";
                $message->reply($reply);
            }, function (\Exception $e) {
                echo "Error: {$e->getMessage()}\n";
            });

            return null;
        }

        if ($message->content === '!leaderboard' || $message->content === '!lb') {
            $betService->getLeaderboard($message->guild_id)->then(function ($result) use ($message) {
                $reply = '';
                foreach ($result as $place => $row) {
                    if ($place === 0) {
                        $reply .= "🥇 ";
                    } elseif ($place === 1) {
                        $reply .= "🥈 ";
                    } elseif ($place === 2) {
                        $reply .= "🥉 ";
                    } else {
                        $reply .= "{$place} ";
                    }

                    $reply .= "{$row['username']} - {$row['points']}\n";
                }
                $message->reply($reply);
            }, function (\Exception $e) {
                echo "Error: {$e->getMessage()}\n";
            });

            return null;
        }

        return null;
    }
}