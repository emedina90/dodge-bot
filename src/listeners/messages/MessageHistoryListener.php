<?php

namespace Ericmedina\DodgeBot\listeners\messages;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\services\MessageHistoryServiceContract;
use React\Promise\PromiseInterface;
use function React\Async\async;
use function React\Async\await;

class MessageHistoryListener implements MessageListener
{
    public function execute(Message $message, Discord $discord): ?PromiseInterface
    {
        /** @var MessageHistoryServiceContract $messageHistoryService */
        $messageHistoryService = Container::get(MessageHistoryServiceContract::class);

        async(function () use ($messageHistoryService, $message) {
            try {
                await($messageHistoryService->store($message));
            } catch (\Exception $exception) {
                Logger::log("Error: ". $exception->getMessage());
            }
        })();

        return null;
    }
}