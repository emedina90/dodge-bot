<?php namespace Ericmedina\DodgeBot\listeners\messages;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use React\Promise\PromiseInterface;

interface MessageListener
{
    public function execute(Message $message, Discord $discord): ?PromiseInterface;
}