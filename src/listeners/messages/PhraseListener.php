<?php

namespace Ericmedina\DodgeBot\listeners\messages;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Ericmedina\DodgeBot\actions\EchoAction;
use Ericmedina\DodgeBot\constants\DodgeConstants;
use Ericmedina\DodgeBot\services\GiphyService;
use React\Promise\PromiseInterface;

class PhraseListener implements MessageListener
{
    public function execute(Message $message, Discord $discord): ?PromiseInterface
    {
        $hasPhraseInMessage = false;
        $phrase             = null;

        foreach (DodgeConstants::getAll() as $key => $value) {
            if (str_contains(strtolower($message->content), $value)) {
                $phrase = $value;
                $hasPhraseInMessage = true;
                break;
            }
        }

        if (!$hasPhraseInMessage) return null;

        $giphy = new GiphyService();
        return $giphy->searchAndRandomFirst($phrase, 25)
            ->then(function($url) use ($message, $discord) {
                (new EchoAction('', $url))
                    ->run($message, $discord);
            });
    }
}