<?php

namespace Ericmedina\DodgeBot\listeners\messages;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\services\BetService;
use React\Promise\PromiseInterface;

class BetListener implements MessageListener
{
    public function execute(
        Message $message,
        Discord $discord
    ): ?PromiseInterface {
        $matches = $this->parseBetString($message->content);

        if (empty($matches)) {
            return null;
        }

        $team = $matches["team"];
        $win = $matches["win"];
        $spread = (int) $matches["spread"];
        $userName = $message->author->username;
        $guildId = $message->guild_id;

        /** @var BetService $betService */
        $betService = Container::get(BetService::class);

        Logger::log(
            "Bet placed by {$userName} for {$team} to {$win} by {$spread} points"
        );

        $betService
            ->processBet($guildId, $userName, $team, $win, $spread)
            ->then(
                function ($result) use ($message, $team) {
                    $message->reply($result);

                    if (str_starts_with($result, "Bet placed:")) {
                        $wagerMessage =
                            ":new: Feelin' lucky and want a chance to wager some points?";
                        $wagerMessage .= " Try something like `!wager 25 on $team` to put your points on the line!";
                        $wagerMessage .=
                            " Win and double your wager, otherwise lose it all!";

                        $message->reply($wagerMessage);
                    }
                },
                function (\Exception $e) {
                    echo "Error: {$e->getMessage()}\n";
                }
            );

        return null;
    }

    private function parseBetString(string $betString): array
    {
        $regex =
            '/^!bet\s+(?<team>\w+)\s+(?<win>(win|lose))\s+by\s+(?<spread>\d+)$/';
        $matches = [];
        preg_match($regex, $betString, $matches);

        return $matches;
    }
}
