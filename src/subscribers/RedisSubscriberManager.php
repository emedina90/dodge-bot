<?php

namespace Ericmedina\DodgeBot\subscribers;

use Ericmedina\DodgeBot\Logger;

class RedisSubscriberManager implements SubscriberManager {
    public function start(\React\EventLoop\LoopInterface $loop): void 
    {
        Logger::log("Starting Redis Subscriber Manager...");
    }
}