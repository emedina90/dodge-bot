<?php 

namespace Ericmedina\DodgeBot\subscribers;

interface Subscriber {
    public function handle(array $data);
}