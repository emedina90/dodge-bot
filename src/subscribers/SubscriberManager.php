<?php

namespace Ericmedina\DodgeBot\subscribers;

use React\EventLoop\LoopInterface;

interface SubscriberManager {
    public function start(LoopInterface $loop): void;
}