<?php

namespace Ericmedina\DodgeBot\services;

use Clue\React\Redis\Client;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\models\nba\Game;
use Ericmedina\DodgeBot\models\nba\OpenBet;
use Ericmedina\DodgeBot\models\nba\Points;
use Ericmedina\DodgeBot\models\nba\User;
use React\Cache\CacheInterface;
use React\Http\Browser;
use React\Promise\PromiseInterface;
use function React\Async\await;
use function React\Async\async;

class NbaBetService implements BetService
{
    public function __construct(
        protected CacheInterface $cache,
        protected Client $client
    ) {
    }

    public function getPlayerPoints(
        string $guildId,
        string $userName
    ): PromiseInterface {
        return async(function () use ($guildId, $userName) {
            try {
                return (int) await(
                    $this->client->zscore("leaderboard:$guildId", $userName)
                );
            } catch (\Exception $exception) {
                Logger::log($exception->getMessage());

                throw $exception;
            }
        })();
    }

    public function processBet(
        string $guildId,
        string $userName,
        string $team,
        string $win,
        int $spread
    ): PromiseInterface {
        return async(function () use (
            $guildId,
            $userName,
            $team,
            $win,
            $spread
        ) {
            try {
                $team = ucfirst(strtolower($team));

                $gameId = await($this->getActiveTeamGameId($team));

                if (empty($gameId)) {
                    return "No active game found for that team.";
                }

                /** @var Game $game */
                $game = await($this->getGameFromGameId($gameId));

                Logger::log("GameId: $game->game_id, Period: $game->period");

                if (empty($game) || $game->period > 1) {
                    return "No active game found for that team or game is beyond Q1.";
                }

                /** @var User $user */
                $user = await($this->getOrCreateUser($guildId, $userName));

                $openBet = await(
                    $this->createOrUpdateOpenBet(
                        $guildId,
                        $game,
                        $user->username,
                        $team,
                        $win,
                        $spread
                    )
                );

                return "Bet placed: $openBet->team $openBet->win by $openBet->spread";
            } catch (\Exception $exception) {
                Logger::log($exception->getMessage());

                throw $exception;
            }
        })();
    }

    private function createOrUpdateOpenBet(
        string $guildId,
        Game $game,
        string $userName,
        string $team,
        string $win,
        int $spread
    ): PromiseInterface {
        return async(function () use (
            $guildId,
            $game,
            $userName,
            $team,
            $win,
            $spread
        ) {
            try {
                $key = "open_bets:$userName:$game->game_id";

                $updateArray = [
                    "guildId" => $guildId,
                    "gameId" => $game->game_id,
                    "userName" => $userName,
                    "team" => $team,
                    "win" => $win,
                    "spread" => $spread,
                ];

                Logger::log(
                    "Updating open bet for $userName: $team $win by $spread"
                );

                $openBetsJson = await($this->cache->get($key));

                $openBetsArray = [];

                if (!empty($openBetsJson)) {
                    Logger::log("Found open bets for $userName");

                    $openBetsArray = json_decode($openBetsJson, true);
                }

                $openBetsArray = array_filter(
                    $openBetsArray,
                    fn($openBet) => $openBet["guildId"] !==
                        $updateArray["guildId"]
                );

                $openBetsArray[] = $updateArray;

                await($this->cache->set($key, json_encode($openBetsArray)));

                Logger::log("Updated open bet for $userName");

                $this->addUserNameToGamesWithBets($game, $userName);

                return new OpenBet($updateArray);
            } catch (\Exception $exception) {
                Logger::log($exception->getMessage());
                throw $exception;
            }
        })();
    }

    private function getOrCreateUser(
        string $guildId,
        string $userName
    ): PromiseInterface {
        return async(function () use ($guildId, $userName) {
            try {
                $key = "users:$userName";

                $userJson = await($this->cache->get($key));

                if (empty($userJson)) {
                    Logger::log("No user found for $userName");

                    $user = new User([
                        "username" => $userName,
                        "points" => [
                            new Points([
                                "guildId" => $guildId,
                                "points" => 0,
                            ]),
                        ],
                    ]);

                    Logger::log("Creating user $user->username");

                    await($this->cache->set($key, $user->toJson()));

                    return $user;
                }

                Logger::log("Found user for $userName");

                return User::fromJson($userJson);
            } catch (\Exception $exception) {
                Logger::log($exception->getMessage());

                throw $exception;
            }
        })();
    }

    private function getGameFromGameId(string $gameId): PromiseInterface
    {
        return async(function () use ($gameId) {
            $key = "games:$gameId";

            $gameJson = await($this->cache->get($key));

            if (empty($gameJson)) {
                Logger::log("No game found for $gameId");

                return null;
            }

            Logger::log("Found game for $gameJson");

            return Game::fromJson($gameJson);
        })();
    }

    private function getActiveTeamGameId(string $teamName): PromiseInterface
    {
        return async(function () use ($teamName) {
            $key = "active_teams:$teamName";

            return await($this->cache->get($key));
        })();
    }

    private function addUserNameToGamesWithBets(Game $game, string $userName)
    {
        async(function () use ($game, $userName) {
            $key = "games_with_bets:$game->game_id";

            $gamesWithBetsJson = await($this->cache->get($key));

            $gamesWithBetsArray = [];

            if (!empty($gamesWithBetsJson)) {
                Logger::log("Found games with bets");

                $gamesWithBetsArray = json_decode($gamesWithBetsJson, true);
            }

            if (in_array($userName, $gamesWithBetsArray)) {
                Logger::log("User $userName already in games with bets");

                return;
            }

            $gamesWithBetsArray[] = $userName;

            Logger::log("Adding user $userName to games with bets");

            $this->cache->set(
                $key,
                json_encode($gamesWithBetsArray),
                60 * 60 * 24
            );
        })();
    }

    public function getLeaderboard(string $guildId): PromiseInterface
    {
        return async(function () use ($guildId) {
            $key = "leaderboard:$guildId";

            $leaderboard = await(
                $this->client->zrevrange($key, 0, -1, "WITHSCORES")
            );

            $board = [];

            foreach ($leaderboard as $index => $item) {
                if ($index % 2 === 0) {
                    $item = [];
                    $item["username"] = $leaderboard[$index];
                    $item["points"] = $leaderboard[$index + 1];
                    $board[] = $item;
                }
            }

            return $board;
        })();
    }

    public function processWager(
        string $guildId,
        string $userName,
        int $wager,
        string $team
    ): PromiseInterface {
        return async(function () use ($guildId, $userName, $wager, $team) {
            /**
             * @var Browser $client
             *
             * We'll make this a specific client wrapper later.
             */
            $client = Container::get(Browser::class);
            $url = "http://localhost:3000/v1/nba/wager";

            $data = [
                "username" => $userName,
                "guildId" => $guildId,
                "wager" => $wager,
                "team" => $team,
            ];

            try {
                Logger::log("Sending HTTP request to Golang service.");
                $response = await(
                    $client->post(
                        $url,
                        [
                            "Content-Type" => "application/json",
                        ],
                        json_encode($data)
                    )
                );

                $body = (string) $response->getBody();

                Logger::log("Received response from Golang service: $body");

                $json = json_decode($body, true);

                return $json["message"] ?? "";
            } catch (\Throwable $exception) {
                Logger::log(
                    "Error talking with Golang service: {$exception->getMessage()}"
                );
                return "Sorry, there was a problem.";
            }
        })();
    }
}
