<?php

namespace Ericmedina\DodgeBot\services;

use Discord\Parts\Channel\Message;
use Ericmedina\DodgeBot\queues\models\QueueMessage;
use Ericmedina\DodgeBot\queues\QueueClient;
use React\Cache\CacheInterface;
use function React\Async\await;
use function React\Async\async;

class TarsService implements TarsServiceContract
{
    public function __construct(
        protected string $queueUrl,
        protected QueueClient $queueClient,
        protected CacheInterface $cache,
    )
    {
    }

    public function dispatch(Message $message): void
    {
        async(function() use ($message) {
            $messageId = uniqid("tars-messages:");

            $guildChannelUserKey = implode(":", [
                $message->channel->guild_id,
                $message->channel_id,
                $message->id,
            ]);

            await($this->cache->set($messageId, $guildChannelUserKey, 300));

            $queueMessage = new QueueMessage([
                'message_id'        => $messageId,
                'question'          => $message->content,
                'sending_user_name' => $message->author->username,
                'type'              => $this->isPictureRequest($message->content) ? "image" : "chat",
            ]);

            $this->queueClient->sendMessage($this->queueUrl, $queueMessage);
        })();
    }

    private function isPictureRequest(string $content): bool
    {
        return str_starts_with(strtolower($content), 'tars picture ');
    }
}