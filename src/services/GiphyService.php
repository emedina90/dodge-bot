<?php

namespace Ericmedina\DodgeBot\services;

use Ericmedina\DodgeBot\Container;
use Psr\Http\Message\ResponseInterface;
use React\Http\Browser;
use React\Promise\Deferred;
use React\Promise\PromiseInterface;

class GiphyService implements GifService
{
    CONST BASE_URL = "https://api.giphy.com/v1/gifs/search";

    /**
     * @var Browser $client
     */
    private Browser $client;

    public function __construct()
    {
        /**
         * @var Browser $client
         */
        $client       = Container::get(Browser::class);
        $this->client = $client;
    }

    /**
     * @param string $searchString
     * @param int $limit
     * @return PromiseInterface<ResponseInterface>
     */
    public function searchAndRandomFirst(string $searchString, int $limit): PromiseInterface
    {
        $apiKey   = $_ENV['GIPHY_API_KEY'] ?? '';
        $qs       = "?api_key=$apiKey&q=$searchString&limit=$limit&lang=en";

        $deferred = new Deferred();

        $this->client->get(self::BASE_URL.$qs)
            ->then(function(ResponseInterface $response) use ($deferred) {
                $json      = json_decode((string)$response->getBody(), true);

                $randomKey = array_rand($json['data'] ?? []);

                $randomUrl = $json['data'][$randomKey]['images']['original']['url'];

                $deferred->resolve($randomUrl);
            }, function(\Exception $exception) use ($deferred) {
                echo "Error: " . $exception->getMessage() . PHP_EOL;

                $deferred->reject($exception->getMessage());
            });

        return $deferred->promise();
    }
}