<?php

namespace Ericmedina\DodgeBot\services;

use Discord\Parts\Channel\Message;
use React\Promise\PromiseInterface;

interface MessageHistoryServiceContract
{
    public function store(Message $message): PromiseInterface;
}