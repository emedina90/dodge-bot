<?php

namespace Ericmedina\DodgeBot\services;

use Discord\Parts\Channel\Message;

interface TarsServiceContract
{
    public function dispatch(Message $message): void;
}