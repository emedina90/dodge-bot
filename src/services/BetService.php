<?php

namespace Ericmedina\DodgeBot\services;

use React\Promise\PromiseInterface;

interface BetService
{
    public function processBet(string $guildId, string $userName, string $team, string $win, int $spread): PromiseInterface;

    public function getPlayerPoints(string $guildId, string $userName): PromiseInterface;

    public function getLeaderboard(string $guildId): PromiseInterface;

    public function processWager(string $guildId, string $userName, int $wager, string $team): PromiseInterface;
}