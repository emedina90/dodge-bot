<?php

namespace Ericmedina\DodgeBot\services;

use Clue\React\Redis\Client;
use Discord\Parts\Channel\Message;
use Ericmedina\DodgeBot\Logger;
use React\Promise\PromiseInterface;
use function React\Async\async;
use function React\Async\await;

class MessageHistoryService implements MessageHistoryServiceContract
{
    public function __construct(
        protected Client $client
    )
    {
    }

    public function store(Message $message): PromiseInterface
    {
        return async(function() use ($message) {
            Logger::log("I'm in the new async!");
            await($this->client->rpush("message-history:$message->guild_id", $this->formatMessage($message)));
            await($this->client->ltrim("message-history:$message->guild_id", -10, -1));
        })();
    }

    private function formatMessage(Message $message): string
    {
        $username = $message->user->username;

        return "$username said $message->content";
    }
}