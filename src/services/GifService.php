<?php

namespace Ericmedina\DodgeBot\services;

interface GifService
{
    public function searchAndRandomFirst(string $searchString, int $limit);
}