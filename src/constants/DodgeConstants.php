<?php

namespace Ericmedina\DodgeBot\constants;

final class DodgeConstants
{
    const DODGE   = 'dodg';
    const CRACKED = 'cracked';
    const SHOCKED = 'shocked';
    const BLAZE   = 'blaze it';
    const CHAMPS  = 'champs';
    const APEX    = 'apex';

    public static function getAll(): array
    {
        $reflection = new \ReflectionClass(DodgeConstants::class);

        return $reflection->getConstants();
    }
}