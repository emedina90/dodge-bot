<?php

namespace Ericmedina\DodgeBot\models\nba;

use Ericmedina\DodgeBot\models\BaseModel;

class OpenBet extends BaseModel
{
    public string $guildId;
    public string $gameId;
    public string $userName;
    public string $team;
    public string $win;
    public int $spread;
}