<?php

namespace Ericmedina\DodgeBot\models\nba;

use Ericmedina\DodgeBot\models\BaseModel;

class Points extends BaseModel
{
    public string $guildId;

    public int $points;
}