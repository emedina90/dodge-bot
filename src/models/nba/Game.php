<?php

namespace Ericmedina\DodgeBot\models\nba;

use Ericmedina\DodgeBot\models\BaseModel;

class Game extends BaseModel
{
    public string $game_id;

    public int $game_status;

    public int $period;
}