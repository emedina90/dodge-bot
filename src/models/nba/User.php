<?php

namespace Ericmedina\DodgeBot\models\nba;

use Ericmedina\DodgeBot\models\BaseModel;

class User extends BaseModel
{
    public string $username;
    /** @var array|Points[] */
    public array $points;
}