<?php

namespace Ericmedina\DodgeBot\models;

use JsonSerializable;

abstract class BaseModel implements JsonSerializable
{
    public function __construct(array $data)
    {
        $this->hydrate($data);
    }

    public function jsonSerialize(): array
    {
        $classProperties = get_object_vars($this);

        $output = [];

        foreach ($classProperties as $property => $value) {
            $output[$property] = $value;
        }

        return $output;
    }

    public function toJson(): string
    {
        return json_encode($this);
    }

    public static function fromJson(string $json): static
    {
        $jsonArray = json_decode($json, true);

        return new static($jsonArray);
    }

    private function hydrate(array $data): void
    {
        $classProperties = get_class_vars(get_class($this));

        foreach ($classProperties as $property => $value) {
            $this->$property = $data[$property] ?? null;
        }
    }
}