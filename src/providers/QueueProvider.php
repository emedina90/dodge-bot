<?php

namespace Ericmedina\DodgeBot\providers;

use Ericmedina\DodgeBot\queues\AwsSqsClient;
use Ericmedina\DodgeBot\queues\QueueManager;
use Ericmedina\DodgeBot\queues\SqsQueueManager;
use League\Container\ServiceProvider\AbstractServiceProvider;

class QueueProvider extends AbstractServiceProvider
{
    public function provides(string $id): bool
    {
        $services = [
            QueueManager::class
        ];

        return in_array($id, $services);
    }

    public function register(): void
    {
        $this->getContainer()
            ->add(QueueManager::class, function() {
                $sqsClient = new \Aws\Sqs\SqsClient([
                    'region'      => getenv('AWS_REGION') ?? 'us-east-1',
                    'version'     => 'latest',
                    'credentials' => \Aws\Credentials\CredentialProvider::env()
                ]);

                $awsSqsClient = new AwsSqsClient($sqsClient);

                return new SqsQueueManager($awsSqsClient);
            });
    }
}