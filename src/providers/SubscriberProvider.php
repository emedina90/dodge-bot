<?php

namespace Ericmedina\DodgeBot\providers;

use Ericmedina\DodgeBot\subscribers\RedisSubscriberManager;
use Ericmedina\DodgeBot\subscribers\SubscriberManager;
use League\Container\ServiceProvider\AbstractServiceProvider;

class SubscriberProvider extends AbstractServiceProvider
{
    public function provides(string $id): bool
    {
        $services = [
            SubscriberManager::class
        ];

        return in_array($id, $services);
    }

    public function register(): void
    {
        $this
            ->getContainer()
            ->add(SubscriberManager::class, function() {
                return new RedisSubscriberManager();
            });

    }
}