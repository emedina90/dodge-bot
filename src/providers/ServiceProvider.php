<?php

namespace Ericmedina\DodgeBot\providers;

use Clue\React\Redis\Factory;
use Ericmedina\DodgeBot\queues\AwsSqsClient;
use Ericmedina\DodgeBot\services\BetService;
use Ericmedina\DodgeBot\services\MessageHistoryService;
use Ericmedina\DodgeBot\services\MessageHistoryServiceContract;
use Ericmedina\DodgeBot\services\NbaBetService;
use Ericmedina\DodgeBot\services\TarsService;
use Ericmedina\DodgeBot\services\TarsServiceContract;
use League\Container\ServiceProvider\AbstractServiceProvider;
use React\Cache\CacheInterface;

class ServiceProvider extends AbstractServiceProvider
{
    public function provides(string $id): bool
    {
        $services = [
            BetService::class,
            TarsServiceContract::class,
            MessageHistoryServiceContract::class,
        ];

        return in_array($id, $services);
    }

    public function register(): void
    {
        $factory = new Factory();
        $redis = $factory->createLazyClient('localhost:6379');

        $this->getContainer()->add(BetService::class, function () use ($redis) {
            return new NbaBetService(
                $this->getContainer()->get(CacheInterface::class),
                $redis
            );
        });

        $this->getContainer()->add(TarsServiceContract::class, function () {
            $sqsClient = new \Aws\Sqs\SqsClient([
                'region'      => getenv('AWS_REGION') ?? 'us-east-1',
                'version'     => 'latest',
                'credentials' => \Aws\Credentials\CredentialProvider::env()
            ]);

            $awsSqsClient = new AwsSqsClient($sqsClient);

            return new TarsService(
                $_ENV['TARS_INQUIRY_QUEUE_URL'],
                $awsSqsClient,
                $this->getContainer()->get(CacheInterface::class)
            );
        });

        $this->getContainer()->add(MessageHistoryServiceContract::class, function() use ($redis) {
            return new MessageHistoryService(
                $redis
            );
        });
    }
}