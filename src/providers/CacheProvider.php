<?php

namespace Ericmedina\DodgeBot\providers;

use Clue\React\Redis\Factory;
use Ericmedina\DodgeBot\cache\RedisCache;
use League\Container\ServiceProvider\AbstractServiceProvider;
use React\Cache\ArrayCache;
use React\Cache\CacheInterface;

class CacheProvider extends AbstractServiceProvider
{
    public function provides(string $id): bool
    {
        $services = [
            CacheInterface::class
        ];

        return in_array($id, $services);
    }

    public function register(): void
    {
        $this
            ->getContainer()
            ->add(CacheInterface::class, function() {
                $factory = new Factory();
                $redis = $factory->createLazyClient('localhost:6379');

                return new RedisCache($redis);
            });

    }
}