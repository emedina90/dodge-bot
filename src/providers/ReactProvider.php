<?php

namespace Ericmedina\DodgeBot\providers;

use Discord\Discord;
use Ericmedina\DodgeBot\Container;
use League\Container\ServiceProvider\AbstractServiceProvider;
use React\EventLoop\LoopInterface;
use React\Http\Browser;

class ReactProvider extends AbstractServiceProvider
{
    private LoopInterface $loop;
    private Discord $discord;

    public function __construct(LoopInterface $loop, Discord $discord)
    {
        $this->loop    = $loop;
        $this->discord = $discord;
    }

    public function provides(string $id): bool
    {
        $services = [
            LoopInterface::class,
            Browser::class,
            Discord::class,
        ];

        return in_array($id, $services);
    }

    public function register(): void
    {
        $this
            ->getContainer()
            ->add(LoopInterface::class, function() {
                return $this->loop;
            });

        $this
            ->getContainer()
            ->add(Discord::class, function() {
                return $this->discord;
            });

        $this
            ->getContainer()
            ->add(Browser::class, function() {
                return new Browser(
                    null,
                    Container::get(LoopInterface::class)
                );
            });
    }
}