<?php

namespace Ericmedina\DodgeBot\actions;

use Ericmedina\DodgeBot\BaseCollection;

class DiscordActionsCollection extends BaseCollection
{
    function ofType(): string
    {
        return DiscordAction::class;
    }
}