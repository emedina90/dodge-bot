<?php

namespace Ericmedina\DodgeBot\actions;

use Discord\Discord;
use Discord\Parts\Channel\Message;

interface DiscordAction
{
    public function run(Message $message, Discord $discord);
}