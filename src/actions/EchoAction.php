<?php

namespace Ericmedina\DodgeBot\actions;

use Discord\Builders\MessageBuilder;
use Discord\Discord;
use Discord\Parts\Channel\Message;
use Discord\Parts\Embed\Embed;

class EchoAction implements DiscordAction
{
    private string $message;
    private ?string $giphyUrl;

    public function __construct(
        string $message,
        ?string $giphyUrl = null
    )
    {
        $this->message = $message;
        $this->giphyUrl = $giphyUrl;
    }

    public function run(Message $message, Discord $discord)
    {
        $messageToSend = MessageBuilder::new()
            ->setContent($this->message);

        if ($this->giphyUrl) {
            $embed = new Embed($discord);
            $embed
                ->setImage("$this->giphyUrl");

            $messageToSend->setEmbeds([$embed]);
        }

        $message->channel->sendMessage($messageToSend)->done(function (Message $message) {
            echo "Message sent!", PHP_EOL;
        });
    }
}