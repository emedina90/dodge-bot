<?php

namespace Ericmedina\DodgeBot;

use Discord\Discord;
use Ericmedina\DodgeBot\providers\CacheProvider;
use Ericmedina\DodgeBot\providers\QueueProvider;
use Ericmedina\DodgeBot\providers\ReactProvider;
use Ericmedina\DodgeBot\providers\ServiceProvider;
use Ericmedina\DodgeBot\providers\SubscriberProvider;
use React\EventLoop\LoopInterface;

class Container
{
    private static \League\Container\Container $container;

    private static bool $initialized = false;

    private static LoopInterface $loop;

    private static Discord $discord;

    public static function init(LoopInterface $loop, \Discord\Discord $discord): \League\Container\Container
    {
        self::$loop = $loop;
        self::$discord = $discord;

        if (empty(self::$container)) {
            self::$container   = new \League\Container\Container();
        }

        if (!self::$initialized) {
            self::initialize();
        }

        return self::$container;
    }

    /**
     * Get a dependency from the Container
     */
    public static function get(string $className)
    {
        return self::$container->get($className);
    }

    /**
     * Initialiaze all /providers dynamnically.
     */
    private static function initialize()
    {
        self::$container
            ->addServiceProvider(new ReactProvider(self::$loop, self::$discord))
            ->addServiceProvider(new QueueProvider())
            ->addServiceProvider(new CacheProvider())
            ->addServiceProvider(new SubscriberProvider())
            ->addServiceProvider(new ServiceProvider());
    }
}