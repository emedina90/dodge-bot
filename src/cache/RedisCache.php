<?php

namespace Ericmedina\DodgeBot\cache;

use Clue\React\Redis\Client;
use React\Cache\CacheInterface;
use React\Promise\Deferred;
use React\Promise\Promise;
use React\Promise\PromiseInterface;

class RedisCache implements CacheInterface
{
    public function __construct(
        protected Client $client
    )
    {
    }

    public function get($key, $default = null): PromiseInterface
    {
        $deferred = new Deferred();

        $this->client->get($key)->then(function ($result) use ($deferred) {
            $deferred->resolve($result);
        }, function(\Exception $exception) use ($deferred) {
            echo "Error: " . $exception->getMessage() . PHP_EOL;

            $deferred->reject($exception->getMessage());
        });

        return $deferred->promise();
    }

    public function set($key, $value, $ttl = null): PromiseInterface
    {
        $deferred = new Deferred();

        $args = [$key, $value];

        if ($ttl !== null) {
            $args[] = "EX";
            $args[] = $ttl;
        }

        $this->client->set(...$args)->then(function ($result) use ($deferred) {
            $deferred->resolve($result);
        }, function(\Exception $exception) use ($deferred) {
            echo "Error Setting Redis Value: " . $exception->getMessage() . PHP_EOL;

            $deferred->reject($exception->getMessage());
        });

        return $deferred->promise();
    }

    public function delete($key)
    {
        $this->client->del($key);
    }

    public function getMultiple(array $keys, $default = null)
    {
        throw new \RuntimeException('Not implemented');
    }

    public function setMultiple(array $values, $ttl = null)
    {
        throw new \RuntimeException('Not implemented');
    }

    public function deleteMultiple(array $keys)
    {
        throw new \RuntimeException('Not implemented');
    }

    public function clear()
    {
        throw new \RuntimeException('Not implemented');
    }

    public function has($key)
    {
        throw new \RuntimeException('Not implemented');
    }
}