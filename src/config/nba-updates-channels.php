<?php

return [
    'guilds' => [
        [
            'id'       => '999170219773804555', // Girthy PCs
            'channels' => [
                [
                    'id' => '1080576870338269293' // Lakers Talk
                ]
            ],
            'emojis' => [
                'Lakers' => '<:lakers:1167117049110216755>',
                'Kings'  => '<:kings:1167117115984199852>',
            ]
        ],
        [
            'id'       => '472619078570475541', // Eric Test Guild
            'channels' => [
                [
                    'id' => '472619079023329293' // General
                ]
            ],
            'emojis' => [
                'Lakers' => '<:lakers:1041588706282307674>'
            ]
        ],
    ]
];
