<?php

namespace Ericmedina\DodgeBot\queues;

use Aws\Exception\AwsException;
use Aws\Sqs\SqsClient;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\queues\models\QueueAttributes;
use Ericmedina\DodgeBot\queues\models\QueueMessage;
use React\Promise\PromiseInterface;

/*
 * This is a SYNCHRONOUS client, so be careful how you use it.
 *
 * Lesson learned, trying to use PHP for async stuff is not the one. Node JS does this, why try in PHP?
 * There is additional complexity and knowledge needed to know how to utilize the React Loop, creating a Guzzle handler
 * and passing that to the aws-sdk-php if you want to truly make this Async. Is that worth it? Next time, stick to Node.js or Go.
 */
class AwsSqsClient implements QueueClient
{
    public function __construct(
        protected SqsClient $sqsClient
    ){}

    public function receiveMessage(QueueAttributes $attributes): ?QueueMessage
    {
        $queueMessage = null;

        try {
            $result = $this->sqsClient->receiveMessage($attributes->toArray());

            Logger::log(json_encode($result->toArray()));

            $messages = $result->get('Messages');

            if (!empty($messages)) {
                $this->sqsClient->deleteMessage([
                    'QueueUrl' => $attributes->queueUrl, // REQUIRED
                    'ReceiptHandle' => $result->get('Messages')[0]['ReceiptHandle'] // REQUIRED
                ]);

                $body    = json_decode($messages[0]['Body'], true);
                $message = json_decode($body['Message'], true);
                $queueMessage = new QueueMessage($message);

                Logger::log("Message received: " . json_encode($queueMessage->body));
            } else {
                echo "No messages found!\n";
            }
        } catch (AwsException $exception) {
            echo "Got an error!: " . $exception->getMessage();
        }

        return $queueMessage;
    }

    public function sendMessage(string $queueUrl, QueueMessage $message): void
    {
        try {
            $this->sqsClient->sendMessage([
                'QueueUrl'    => $queueUrl,
                'MessageBody' => json_encode($message->body),
            ]);
        } catch (AwsException $exception) {
            Logger::log("Got an error!: " . $exception->getMessage());
        }
    }
}