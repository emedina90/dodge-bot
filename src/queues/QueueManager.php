<?php

namespace Ericmedina\DodgeBot\queues;

use React\EventLoop\LoopInterface;

interface QueueManager
{
    public function start(LoopInterface $loop): void;
}