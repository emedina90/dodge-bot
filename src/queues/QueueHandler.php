<?php

namespace Ericmedina\DodgeBot\queues;

use Ericmedina\DodgeBot\queues\models\QueueAttributes;
use Ericmedina\DodgeBot\queues\models\QueueMessage;

interface QueueHandler
{
    public function handle(QueueMessage $message): void;

    public function getQueueAttributes(): QueueAttributes;
}