<?php

namespace Ericmedina\DodgeBot\queues\models;

class QueueAttributes
{
    public function __construct(
        protected int $maxNumberOfMessages,
        protected array $messageAttributeNames,
        public string $queueUrl,
        protected int $waitTimeSeconds,
    )
    { }

    public function toArray(): array
    {
        return [
            'MaxNumberOfMessages'   => $this->maxNumberOfMessages,
            'MessageAttributeNames' => $this->messageAttributeNames,
            'QueueUrl'              => $this->queueUrl,
            'WaitTimeSeconds'       => $this->waitTimeSeconds,
        ];
    }
}