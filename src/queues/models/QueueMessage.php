<?php

namespace Ericmedina\DodgeBot\queues\models;

class QueueMessage
{

    public function __construct(
        public array $body
    )
    {}
}