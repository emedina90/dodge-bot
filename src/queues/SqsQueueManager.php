<?php

namespace Ericmedina\DodgeBot\queues;

use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\queues\handlers\BeerSaleListQueueHandler;
use Ericmedina\DodgeBot\queues\handlers\NbaBetsUpdateQueueHandler;
use Ericmedina\DodgeBot\queues\handlers\NbaGameUpdatesQueueHandler;
use Ericmedina\DodgeBot\queues\handlers\TarsQueueHandler;
use Ericmedina\DodgeBot\queues\handlers\TarsShoutQueueHandler;
use React\Cache\CacheInterface;
use React\EventLoop\LoopInterface;

class SqsQueueManager implements QueueManager
{
    private array $queues= [
        NbaGameUpdatesQueueHandler::class,
        BeerSaleListQueueHandler::class,
        NbaBetsUpdateQueueHandler::class,
        TarsQueueHandler::class,
        TarsShoutQueueHandler::class,
    ];

    public function __construct(
        protected AwsSqsClient $sqsClient
    ){}

    public function start(LoopInterface $loop): void
    {
        $cache = Container::get(CacheInterface::class);

        foreach ($this->queues as $queue) {
            echo "Starting $queue\n";
            /** @var QueueHandler $queueHandler */
            $queueHandler = new $queue($cache);
            $loop->addPeriodicTimer(25, function() use ($queueHandler) {
                echo "Looking for messages on queue " . get_class($queueHandler) . "\n";
                $message = $this->sqsClient->receiveMessage($queueHandler->getQueueAttributes());

                if ($message) {
                    $queueHandler->handle($message);
                }
            });
        }
    }
}