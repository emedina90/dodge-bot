<?php

namespace Ericmedina\DodgeBot\queues\handlers;

use Discord\Builders\MessageBuilder;
use Discord\Discord;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\queues\models\QueueAttributes;
use Ericmedina\DodgeBot\queues\models\QueueMessage;
use Ericmedina\DodgeBot\queues\QueueHandler;

class BeerSaleListQueueHandler implements QueueHandler
{
    public function handle(QueueMessage $message): void
    {
        Logger::log("Handling Beer List Sale!\n");
        $config = include __DIR__ . '/../../config/beer-list-sale-channels.php';

        /** @var Discord $discord */
        $discord = Container::get(Discord::class);

        foreach ($config['guilds'] as $configGuild) {
            $discordGuild = $discord->guilds->get('id', $configGuild['id']);

            foreach ($configGuild['channels'] as $configChannel) {
                $channel = $discordGuild->channels->get('id', $configChannel['id']);
                Logger::log("Found message, sending!");
                $channel->sendMessage($this->buildMessage($message, $configGuild));
            }
        }
    }

    public function getQueueAttributes(): QueueAttributes
    {
        return new QueueAttributes(
            1,
            ['All'],
            getenv('BEER_LIST_QUEUE_URL'),
            0
        );
    }

    private function buildMessage(QueueMessage $message, array $configGuild): MessageBuilder
    {
        $maxBeersPerProvider = 5;

        $stringMessage = ":beers: :beers: :beers: Ayo! It's Friday aka Beer Day!\n\n";
        $stringMessage.= "Here's some sales found @ your local joints):\n";

        $sortedBeersByProvider = $this->sortBeersByProvider($message->body);
        
        foreach ($sortedBeersByProvider as $provider => $beerItems) {
            $storeEmoji = $configGuild['emojis'][$provider] ?? ':convenience_store:';
            $stringMessage.= "$storeEmoji - $provider\n";

            $slicedBeers = array_slice($beerItems, 0, $maxBeersPerProvider);
            foreach ($slicedBeers as $beerItem) {
                $stringMessage .= ":beer: " . $beerItem['name'];
                $stringMessage .= " . . . ";
                $stringMessage .= "$" . number_format((float)$beerItem['price'], 2, '.', '');
                $stringMessage .= "\n";
            }
            $stringMessage .= "\n";
        }

        $stringMessage .= "What ya'll drinkin' this weekend? Cheers! :beer:";

        return MessageBuilder::new()
            ->setContent($stringMessage);
    }

    private function sortBeersByProvider(array $beerList): array
    {
        $beerMap  = [];

        foreach ($beerList as $beerItem) {
            $provider = $beerItem['provider'] ?? 'Near You';

            if (!array_key_exists($provider, $beerMap)) {
                $beerMap[$provider] = [];
            }

            $beerMap[$provider][] = $beerItem;
        }

        return $beerMap;
    }
}