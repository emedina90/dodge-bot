<?php

namespace Ericmedina\DodgeBot\queues\handlers;

use Discord\Discord;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\queues\models\QueueAttributes;
use Ericmedina\DodgeBot\queues\models\QueueMessage;
use Ericmedina\DodgeBot\queues\QueueHandler;

class NbaBetsUpdateQueueHandler implements QueueHandler
{
    public function handle(QueueMessage $message): void
    {
            Logger::log("Handling Nba Bets Update!\n");
            $config = include __DIR__ . '/../../config/nba-updates-channels.php';
            /** @var Discord $discord */
            $discord = Container::get(Discord::class);

            foreach ($message->body as $winningBet) {
                foreach ($winningBet['won_bets'] as $wonBet) {
                    $channelId = $this->getChannelId($config, $wonBet['bet']['guildId']);

                    if (empty($channelId)) {
                        Logger::log("Channel not found for guild {$wonBet['bet']['guildId']}\n");
                        continue;
                    }

                    $channel = $discord->getChannel($channelId);

                    if (empty($channel)) {
                        Logger::log("Channel not found for id {$channelId}\n");
                        continue;
                    }

                    $str = $this->buildMessage($wonBet);

                    $channel->sendMessage($str);
                    Logger::log("Sent message to channel {$channelId}\n");
                }
            }
    }

    public function getQueueAttributes(): QueueAttributes
    {
        return new QueueAttributes(
            1,
            ['All'],
            getenv('NBA_BETS_UPDATE_QUEUE_URL'),
            0
        );
    }

    private function getChannelId(array $config, string $guildId): string
    {
        foreach ($config['guilds'] as $guild) {
            if ($guild['id'] === $guildId) {
                return $guild['channels'][0]['id'];
            }
        }

        return '';
    }

    private function buildMessage(array $wonBet): string
    {
        $didWin = $wonBet['points_won'] > 0;
        if ($didWin) {
            $message = ":money_with_wings:";
        } else {
            $message = ":small_red_triangle_down:";
        }

        $message .= " {$wonBet['bet']['username']} {$wonBet['bet']['team']} bet: +{$wonBet['points_won']}";

        $message .= $didWin ? " :coin:" : "";

        if (isset($wonBet['did_win_bonus']) && $wonBet['did_win_bonus']) {
            $message .= " :coin:";
            if (isset($wonBet['multiplier']) && $wonBet['multiplier'] > 0) {
                $message .= " :coin:";
            }
        }

        if (isset($wonBet['wagered_points_gained']) && $wonBet['wagered_points_gained'] != 0) {
            $pointsGained = $wonBet['wagered_points_gained'];

            $message .= " :slot_machine: Wager won! Win $pointsGained!";
        } elseif (isset($wonBet['wagered_points_lost']) && $wonBet['wagered_points_lost'] != 0) {
            $pointsLost = $wonBet['wagered_points_lost'];

            $message .= " :chart_with_downwards_trend: Wager lost! Lose $pointsLost!";
        }

        return $message;
    }
}