<?php

namespace Ericmedina\DodgeBot\queues\handlers;

use Discord\Discord;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\queues\models\QueueAttributes;
use Ericmedina\DodgeBot\queues\models\QueueMessage;
use Ericmedina\DodgeBot\queues\QueueHandler;
use React\Cache\CacheInterface;
use function React\Async\async;
use function React\Async\await;

class TarsQueueHandler implements QueueHandler
{
    public function __construct(
        protected CacheInterface $cache
    )
    {
    }

    public function handle(QueueMessage $message): void
    {
        async(function() use ($message) {
            Logger::log("Handling TARS message.");
            /** @var Discord $discord */
            $discord = Container::get(Discord::class);

            Logger::log("Message: " . json_encode($message->body));

            $guildChannelUserKey = await($this->cache->get($message->body['message_id']));

            Logger::log("Sending answer to discord.");

            [$guildId, $channelId, $discordMessageId] = explode(":", $guildChannelUserKey);

            Logger::log("Guild ID: $guildId, Channel ID: $channelId, Message ID: $discordMessageId");

            $discordGuild   = $discord->guilds->get('id', $guildId);

            $channel        = $discordGuild->channels->get('id', $channelId);

            $discordMessage = await($channel->messages->fetch($discordMessageId));

            Logger::log("Message: " . json_encode($message->body));

            $discordMessage->reply($message->body['answer']);
        })();
    }

    public function getQueueAttributes(): QueueAttributes
    {
        return new QueueAttributes(
            1,
            ['All'],
            getenv('TARS_ANSWER_QUEUE_URL'),
            0
        );
    }
}