<?php

namespace Ericmedina\DodgeBot\queues\handlers;

use Discord\Discord;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\queues\models\QueueAttributes;
use Ericmedina\DodgeBot\queues\models\QueueMessage;
use Ericmedina\DodgeBot\queues\QueueHandler;
use React\Cache\CacheInterface;

class TarsShoutQueueHandler implements QueueHandler
{
    public function __construct(
        protected CacheInterface $cache
    )
    {
    }

    public function handle(QueueMessage $message): void
    {
        Logger::log("Handling TARS Shout message.");
        /** @var Discord $discord */
        $discord = Container::get(Discord::class);

        Logger::log("Message: " . json_encode($message->body));

        $discordGuild   = $discord->guilds->get('id', $message->body['guild_id']);
        $channel        = $discordGuild->channels->get('id', $message->body['channel_id']);

        $channel->sendMessage($message->body['shout']);
    }

    public function getQueueAttributes(): QueueAttributes
    {
        return new QueueAttributes(
            1,
            ['All'],
            getenv('TARS_SHOUT_QUEUE_URL'),
            0
        );
    }
}