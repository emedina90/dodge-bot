<?php

namespace Ericmedina\DodgeBot\queues\handlers;

use Discord\Builders\MessageBuilder;
use Discord\Discord;
use Discord\Parts\Channel\Channel;
use Discord\Parts\Channel\Message;
use Discord\Parts\Guild\Guild;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\queues\models\QueueAttributes;
use Ericmedina\DodgeBot\queues\models\QueueMessage;
use Ericmedina\DodgeBot\queues\QueueHandler;
use React\Cache\CacheInterface;
use React\Promise\PromiseInterface;
use function React\Async\async;
use function React\Async\await;

class NbaGameUpdatesQueueHandler implements QueueHandler
{
    private string $cacheKeyNamespace = "nba-game-updates";

    public function __construct(private CacheInterface $cache)
    {
    }

    public function handle(QueueMessage $message): void
    {
        Logger::log("Handling Nba Game Update!\n");
        $config = include __DIR__ . "/../../config/nba-updates-channels.php";
        /** @var Discord $discord */
        $discord = Container::get(Discord::class);

        foreach ($config["guilds"] as $configGuild) {
            $discordGuild = $discord->guilds->get("id", $configGuild["id"]);

            foreach ($configGuild["channels"] as $configChannel) {
                $this->sendGameUpdate(
                    $message,
                    $discordGuild,
                    $configChannel["id"],
                    $configGuild
                );
                $this->sendGameLeadersUpdate(
                    $message,
                    $discordGuild,
                    $configChannel["id"],
                    $configGuild
                );
            }
        }
    }

    private function setCacheWithMessageId(
        Message $discordMessage,
        QueueMessage $message
    ): void {
        async(function () use ($discordMessage, $message) {
            await(
                $this->cache->set(
                    $this->buildCacheKey(
                        $message->body["game_id"],
                        $discordMessage->channel->guild_id,
                        $discordMessage->channel_id
                    ),
                    $discordMessage->id,
                    21600
                )
            );
        })();
    }

    private function buildCacheKey(
        string $gameId,
        string $guildId,
        string $channelId
    ): string {
        return "$this->cacheKeyNamespace:$gameId:$guildId:$channelId:discord-message-id";
    }

    public function getQueueAttributes(): QueueAttributes
    {
        return new QueueAttributes(
            1,
            ["All"],
            getenv("NBA_GAME_UPDATES_QUEUE_URL"),
            0
        );
    }

    /**
     *
     * @param QueueMessage $message
     * @param Guild $discordGuild
     * @param string $targetChannelId
     * @return PromiseInterface<Channel|Message>
     */
    private function getMessageOrChannel(
        QueueMessage $message,
        Guild $discordGuild,
        string $targetChannelId
    ): PromiseInterface {
        $cacheKey = $this->buildCacheKey(
            $message->body["game_id"],
            $discordGuild->id,
            $targetChannelId
        );

        Logger::log("Looking in cache for $cacheKey");

        return async(function () use (
            $cacheKey,
            $discordGuild,
            $targetChannelId
        ) {
            $messageIdFromCache = await($this->cache->get($cacheKey));

            Logger::log(
                "Attempted to find message-id in cache: $messageIdFromCache"
            );

            return await(
                $this->resolveChannelOrMessage(
                    $messageIdFromCache,
                    $discordGuild,
                    $targetChannelId
                )
            );
        })();
    }

    private function resolveChannelOrMessage(
        $messageIdFromCache,
        Guild $discordGuild,
        string $targetChannelId
    ): PromiseInterface {
        return async(function () use (
            $discordGuild,
            $messageIdFromCache,
            $targetChannelId
        ) {
            Logger::log("Checking cache first for existing channel ID!");
            $channel = $discordGuild->channels->get("id", $targetChannelId);

            if (empty($messageIdFromCache)) {
                Logger::log("Cache is empty. Resolving channel!");
                return $channel;
            }

            Logger::log("Looking for thread or channel from cache ID!");

            return await($channel->messages->fetch($messageIdFromCache));
        })();
    }

    private function buildMessage(
        QueueMessage $message,
        array $configGuild
    ): string {
        $homeTeamName = $message->body["home_team"]["team_name"];
        $awayTeamName = $message->body["away_team"]["team_name"];

        $homeTeamEmoji = $configGuild["emojis"][$homeTeamName] ?? "";
        $awayTeamEmoji = $configGuild["emojis"][$awayTeamName] ?? "";

        if ($message->body["game_status"] == 1) {
            $openingMessage = "$awayTeamEmoji$awayTeamName at $homeTeamEmoji$homeTeamName, {$message->body["game_status_text"]}";

            $openingMessage .= "\n\n";
            $openingMessage .= "Try `!bet $homeTeamName win by 10` to make a bet!";

            return $openingMessage;
        }

        $string = "";
        if (
            isset($message->body["away_team"]["score"]) &&
            isset($message->body["home_team"]["score"])
        ) {
            $string .= " $awayTeamEmoji$awayTeamName {$message->body["away_team"]["score"]} - $homeTeamEmoji$homeTeamName {$message->body["home_team"]["score"]}";
            $string .= " {$message->body["game_status_text"]}";
        }

        return $string;
    }

    private function sendGameUpdate(
        QueueMessage $message,
        ?Guild $discordGuild,
        $id,
        mixed $configGuild
    ): void {
        async(function () use ($message, $discordGuild, $id, $configGuild) {
            $messageOrChannel = await(
                $this->getMessageOrChannel($message, $discordGuild, $id)
            );

            if ($messageOrChannel instanceof Channel) {
                Logger::log("Starting a new thread!");

                $thread = await(
                    $messageOrChannel->sendMessage(
                        $this->buildMessage($message, $configGuild)
                    )
                );

                Logger::log("About to setCacheWithMessageId");

                $this->setCacheWithMessageId($thread, $message);

                return;
            }

            $messageOrChannel->edit(
                (new MessageBuilder())->setContent(
                    $this->buildMessage($message, $configGuild)
                )
            );
        })();
    }

    private function sendGameLeadersUpdate(
        QueueMessage $message,
        ?Guild $discordGuild,
        mixed $channelId,
        mixed $configGuild
    ): void {
        if (
            $message->body["game_status"] == 1 ||
            $message->body["period"] < 3
        ) {
            // Only emit at start of 3rd and game over.
            return;
        }

        async(function () use (
            $message,
            $discordGuild,
            $channelId,
            $configGuild
        ) {
            $cacheKey = "game-leaders:{$message->body["game_id"]}:$discordGuild->id:$channelId";

            $cachedPeriodAndStatus = await($this->cache->get($cacheKey));

            $emittablePeriodsAndStatus = ["3:2", "4:3"];
            $currentPeriodAndStatus = "{$message->body["period"]}:{$message->body["game_status"]}";
            $channel = $discordGuild->channels->get("id", $channelId);
            $discordMessage = $this->buildGameLeadersMessage(
                $message,
                $configGuild
            );

            if (empty($discordMessage)) {
                return;
            }

            if (
                !in_array($currentPeriodAndStatus, $emittablePeriodsAndStatus)
            ) {
                return;
            }

            if ($cachedPeriodAndStatus === $currentPeriodAndStatus) {
                return;
            }

            await($channel->sendMessage($discordMessage));

            $this->cache->set($cacheKey, $currentPeriodAndStatus, 86400);
        })();
    }

    private function buildGameLeadersMessage(
        QueueMessage $message,
        mixed $configGuild
    ): string {
        $homeTeamName = $message->body["home_team"]["team_name"];
        $awayTeamName = $message->body["away_team"]["team_name"];

        $homeTeamEmoji = $configGuild["emojis"][$homeTeamName] ?? "";
        $awayTeamEmoji = $configGuild["emojis"][$awayTeamName] ?? "";

        $homeTeamLeader = $message->body["gameLeaders"]["homeLeaders"] ?? [];
        $awayTeamLeader = $message->body["gameLeaders"]["awayLeaders"] ?? [];

        if (empty($homeTeamLeader) || empty($awayTeamLeader)) {
            return "";
        }

        $leadingText = "is leading";

        if ($message->body["game_status"] == 3) {
            $leadingText = "led";
        }

        $discordMessage = "$homeTeamEmoji **{$homeTeamLeader["name"]}** $leadingText **{$homeTeamLeader["teamTricode"]}** with {$homeTeamLeader["points"]} points, {$homeTeamLeader["rebounds"]} rebounds, and {$homeTeamLeader["assists"]} assists!\n";
        $discordMessage .= "$awayTeamEmoji **{$awayTeamLeader["name"]}** $leadingText **{$awayTeamLeader["teamTricode"]}** with {$awayTeamLeader["points"]} points, {$awayTeamLeader["rebounds"]} rebounds, and {$awayTeamLeader["assists"]} assists!";

        return $discordMessage;
    }
}
