<?php

namespace Ericmedina\DodgeBot\queues;

use Ericmedina\DodgeBot\queues\models\QueueAttributes;
use Ericmedina\DodgeBot\queues\models\QueueMessage;
use React\Promise\PromiseInterface;

interface QueueClient
{
    public function receiveMessage(QueueAttributes $attributes): ?QueueMessage;

    public function sendMessage(string $queueUrl, QueueMessage $message): void;
}