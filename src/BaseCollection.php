<?php

namespace Ericmedina\DodgeBot;

abstract class BaseCollection
{
    protected array $items = [];

    public function __construct(array $items = [])
    {
        foreach ($items as $item) {
            if (!is_a($item, $this->ofType())) {
                throw new InvalidCollectionTypeException();
            }
        }

        $this->items = $items;
    }

    abstract function ofType(): string;

    public function addItem($item)
    {
        if (!is_a($item, $this->ofType())) {
            return;
        }

        $this->items[] = $item;
    }

    public function next()
    {
        $currentItem = current($this->items);
        next($this->items);
        return $currentItem;
    }

    public function rewind()
    {
        reset($this->items);
    }

    public function previous()
    {
        return prev($this->items);
    }
}