<?php declare(strict_types=1);

error_reporting(E_ALL &~ E_DEPRECATED);

include __DIR__ . '/../../vendor/autoload.php';

$client = new Predis\Client();

$allUsers = $client->keys('users:*');

foreach ($allUsers as $userKey) {
    $userJson = $client->get($userKey);
    $user = json_decode($userJson, true);
    foreach ($user['points'] as $points) {
        $client->zadd('leaderboard:'.$points['guildId'], [
            $user['username'] => $points['points']
        ]);
    }
}