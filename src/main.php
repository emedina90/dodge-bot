<?php declare(strict_types=1);

error_reporting(E_ALL &~ E_DEPRECATED);

include __DIR__ . '/../vendor/autoload.php';

use Discord\Discord;
use Discord\WebSockets\Intents;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\listeners\DiscordExecutesMessageListeners;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\queues\QueueManager;
use Ericmedina\DodgeBot\subscribers\SubscriberManager;

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__ . '/../');
$dotenv->load();

$discord = new Discord([
    'token' => $_ENV['DISCORD_BOT_TOKEN'] ?? '',
    'loadAllMembers' => true,
    'intents' => Intents::getDefaultIntents() | Intents::GUILD_MEMBERS | Intents::GUILD_PRESENCES,
]);

$loop = $discord->getLoop();

/**
 * Initialize the DI Container
 */
Container::init($loop, $discord);

$listeners = new DiscordExecutesMessageListeners();

$listenersDisabled = (bool) getenv('LISTENERS_DISABLED');

$discord->on('ready', function ($discord) use ($listeners, $listenersDisabled) {
    echo "Bot is ready!", PHP_EOL;

    if (!$listenersDisabled) {
        $discord->on('message', function ($message, $discord) use ($listeners) {
            $listeners->executeListeners($message, $discord);
        });
    } else {
        Logger::log("Listeners are not enabled!");
    }
});

$queueManagerDisabled = (bool) getenv('QUEUE_MANAGER_DISABLED');

if (!$queueManagerDisabled) {
    /** @var QueueManager $queueManager */
    $queueManager = Container::get(QueueManager::class);
    $queueManager->start($loop);
} else {
    Logger::log("Queue manager is not enabled!");
}

$discord->run();