<?php declare(strict_types=1);

error_reporting(E_ALL &~ E_DEPRECATED);

include __DIR__ . '/../vendor/autoload.php';

use Discord\Discord;
use Discord\WebSockets\Intents;
use Ericmedina\DodgeBot\Container;
use Ericmedina\DodgeBot\listeners\DiscordExecutesMessageListeners;
use Ericmedina\DodgeBot\Logger;
use Ericmedina\DodgeBot\models\nba\Points;
use Ericmedina\DodgeBot\models\nba\User;
use Ericmedina\DodgeBot\queues\QueueManager;
use Ericmedina\DodgeBot\services\BetService;
use Ericmedina\DodgeBot\subscribers\SubscriberManager;

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__ . '/../');
$dotenv->load();

$discord = new Discord([
    'token' => $_ENV['DISCORD_BOT_TOKEN'] ?? '',
    'loadAllMembers' => true,
    'intents' => Intents::getDefaultIntents() | Intents::GUILD_MEMBERS | Intents::GUILD_PRESENCES,
]);

$loop = $discord->getLoop();

Container::init($loop, $discord);

$points = new Points([
    'guildId' => '123',
    'points'  => 10,
]);

$user = new User([
    'username' => 'test',
    'points' => [ $points ]
]);

$openBetsJsonArray = '[{"winner": "Lakers"}, {"winner": "Clippers"}]';

$decoded = json_decode($openBetsJsonArray, true);

var_dump($decoded);